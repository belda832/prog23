// FER ÚS DE CLASSPATH (-cp) AL COMPILAR I EXECUTAR, AMB LA RUTA AL JAR DE MYSQL


import java.sql.*;

public class ex2Insert {
    
    public static void main(String[] args) {

        try ( Connection connexio = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/empresa","root","root");
        	Statement stmt = connexio.createStatement();	
        ) {
            /* REGISTRAR EL DRIVER JA NO ÉS NECESSARI EN LES NOVES VERSIONS DEL JAR
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();*/
        
            // Establir la connexió amb la Base de Dades
            System.out.println("Connectant amb la base de dades...");

            System.out.println("Connexió establida amb la base de dades...");
                        
            // CREANT UN OBJECTE STATEMENT
            
            //Statement stmt = conexion.createStatement(); 

            String sql = "CREATE TABLE venedors (" +
                    "id int NOT NULL auto_increment," +
                    "nom varchar(50) NOT NULL default ''," +
                    "data_ingres date NOT NULL default '0000-00-00'," +
                    "salari float NOT NULL default '0'," +
                    "PRIMARY KEY  (id))";
          
            int retorn = stmt.executeUpdate(sql);
            System.out.println("El valor retornat per executeUpdate és " + retorn);
 
            // Afegim dades a la taula venedors
            sql = "INSERT INTO venedors VALUES (1,'pepe', '2007-01-01', 12000)";
            retorn = stmt.executeUpdate(sql);
            System.out.println("El valor retornat per executeUpdate és " + retorn);
            
            System.out.println("Registre afegit!");

        } catch(SQLException se) {
            se.printStackTrace();
        }
    }
    
}
