/*5. Crear una aplicació a Java que connecte a la base de dades empresa, cree un ResultSet de tipus "scrollable" i mostre el conjunt de factures en ordre invers on aparega l'identificador de factura, la sèrie, el número, la data, el nom del client i el venedor. Després mostrarem, per aquest ordre, les següents files: última, primera, tercera i segona*/
//Por Diego Calatayud

import java.sql.*;
public class ex5
{
	public static void main(String[] args)
	{
	    try(Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa","root","");
	    	Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
	    	ResultSet rs = st.executeQuery("SELECT f.id,f.serie,f.numero,f.fecha,c.nombre,v.nombre FROM facturas f INNER JOIN clientes c on c.id=f.cliente INNER JOIN vendedores v on v.id=f.vendedor;"))
	    {
	    	int id;
	    	String serie;
	    	int num;
	    	String date;
	    	String cliente;
	    	String vendedor;
	    	System.out.println("Connectant amb la base de dades...");

	    	rs.last(); //Em posicione al final
	    	id=rs.getInt(1);
	    	serie=rs.getString(2);
	    	num=rs.getInt(3);
	    	date=rs.getString(4);
	    	cliente=rs.getString(5);
	    	vendedor=rs.getString(6);
	    	System.out.println("--RESULTAT DE LA CONSULTA--");
	    	System.out.println("ID-SERIE-NUMERO-DATA-CLIENT-VENEDOR--");
	    	System.out.println(id+" | "+serie+" | "+num+" | "+date+" | "+cliente+" | "+vendedor);

	    	rs.first(); //Em posicione al principi
	    	id=rs.getInt(1);
	    	serie=rs.getString(2);
	    	num=rs.getInt(3);
	    	date=rs.getString(4);
	    	cliente=rs.getString(5);
	    	vendedor=rs.getString(6);
	    	System.out.println(id+" | "+serie+" | "+num+" | "+date+" | "+cliente+" | "+vendedor);

	    	rs.absolute(3); //Em posicione en la tercera.
	    	id=rs.getInt(1);
	    	serie=rs.getString(2);
	    	num=rs.getInt(3);
	    	date=rs.getString(4);
	    	cliente=rs.getString(5);
	    	vendedor=rs.getString(6);
	    	System.out.println(id+" | "+serie+" | "+num+" | "+date+" | "+cliente+" | "+vendedor);

	    	//rs.absolute(2); //Emp posicione en la segona.
	    	rs.relative(-1);
	    	// rs.absolute(-3);
	    	id=rs.getInt(1);
	    	serie=rs.getString(2);
	    	num=rs.getInt(3);
	    	date=rs.getString(4);
	    	cliente=rs.getString(5);
	    	vendedor=rs.getString(6);
	    	System.out.println(id+" | "+serie+" | "+num+" | "+date+" | "+cliente+" | "+vendedor);



	    }
	    catch(SQLException e)
	    {
	    	e.printStackTrace();
	    }
	}
}
