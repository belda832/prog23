//Programa que mostra les 10 taules de multiplicar (per 1,2,3, ..., 10).

public class e10{
  public static void main (String args[]){

    for (int i = 0 ; i < 11 ; i++){
      System.out.println("Tabla del " + i);
      for (int n = 0 ; n < 11 ; n++)
        System.out.println(i + " * " + n + " = " + (i * n));
      System.out.println(" ");
    }
  }
}
