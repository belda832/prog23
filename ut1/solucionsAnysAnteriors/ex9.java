// 9. Programa que accepta 3 nombres i els mostra ordenats.

public class ex9
{
    public static void main(String args[])
    {
        double n1,n2,n3;

        System.out.println("Introdueix 3 valors numèrics:");
        n1 = Double.parseDouble(System.console().readLine());
        n2 = Double.parseDouble(System.console().readLine());
        n3 = Double.parseDouble(System.console().readLine());
        if (n1 < n2)
            if (n2 < n3)
                System.out.println(n1 + " " + n2 + " " + n3);
            else     // n1 < n2  , n3 < n2 
              if (n1 < n3)
                System.out.println(n1 + " " + n3 + " " + n2);
              else  // n3 < n1, n1 < n2  , n3 < n2 
                System.out.println(n3 + " " + n1 + " " + n2);
        else  // n2 < n1
          if (n1 < n3)
            System.out.println(n2 + " " + n1 + " " + n3);
          else  // n2 < n1, n3 < n1
            if (n2 < n3)
              System.out.println(n2 + " " + n3 + " " + n1);
            else  // n2 < n1, n3 < n1, n3 < n2
              System.out.println(n3 + " " + n2 + " " + n1); 
    }
}
