/* 10. Programa que mostra les 10 taules de multiplicar. */


public class ex10
{
    
    public static void main (String args[])
    {
        int n=1;
        {
        for( ; n<=10; n++) 
            for( int fac = 1 ; fac<=10; fac++)
            	System.out.println(n + " x " + fac + " = " + n*fac);
        }    
    } 
}


