; Programa que mostre els 10 primers números naturals

(comment
	(def cont 1)
	(while (<= cont 10)
		(println cont)
		; he d'incrementar el contador
		;(def cont (+ cont 1 ))
		(def cont (inc cont))	; incorrecte des d'el punt de vista del paradigma FUNCIONAL però funciona !
	)
)

(loop [cont 1]
	(println cont)
	(if (< cont 10)
		(recur (inc cont))
	)
)
(print cont)
