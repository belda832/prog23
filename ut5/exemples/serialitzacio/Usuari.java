// Classe a serialitzar
import java.io.*;

class Usuari implements Serializable
{
	private String nom;
	private String email;
	private transient String passwd;	// no volem "serialitzar" la contrasenya

	public Usuari(String n,String e,String p)
	{
		nom=n ; email=e ; passwd=p;
	}

	public void setValors(String n, String e, String p)
	{
		nom=n ; email=e ; passwd=p;
	}
	public String toString()
	{
		return nom + "\t" + email + "\t" + passwd; 
	}
}
