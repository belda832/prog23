// exemple d'ordenació pel mètode de la bambolla (Bubble sort) OPTIMITIZAT

import java.util.Scanner;

public class OrdenaArrayDoubleOptimitzat
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		final int N = 4;
		
		// creem l'array de 4 elements
		double nums[] = new double[N];
		// carregar valors des de teclat
		carregaValors(nums);
		// mostrar valors de l'array en pantalla
		mostraValors(nums);
		// ordene valors
		ordenaValors(nums);
		// torne a mostrar l'array ja ordenat
		System.out.println("Array ja ordenat");
		mostraValors(nums);
	}
	
	public static void carregaValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
		{
			System.out.println("Introduix un valor numèric:");
			nums[i] = ent.nextDouble();
		}
	}
	
	public static void mostraValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
		System.out.println("");
	}
	
	public static void ordenaValors(double nums[])
	{
		int limit = nums.length - 2, i;
		double aux; boolean ordenat=false;
		
		while ((limit >= 0) && (ordenat == false))	// ((limit >= 0) && (!ordenat))
		{
			ordenat = true;
			i = 0;
			while (i <= limit)
			{
				//cont++;
				if (nums[i+1] < nums[i])	// if (nums[i] > nums[i+1])
				{
					ordenat = false;
					aux = nums[i];
					nums[i] = nums[i+1];
					nums[i+1] = aux;
				}
				i++;
			}
			limit--;
		}
				
	}
}
