// Algoritme de recerca DICOTÓMICA (només és vàlid en arrays ORDENATS)
import java.util.Scanner;

public class CercaDicotomica
{
	public static void main(String args[])
	{
		int valor;
		
		Scanner ent = new Scanner(System.in);
		
		int array[] = {5,20,35,50,65,80,95,110,125};
		System.out.println("Introduix valor a cercar:");
		valor = ent.nextInt();
		if (cercaDicotomica(array,valor))
			System.out.println("Si està present en l'array");
		else
			System.out.println("NO està present en l'array");
	}
	
	public static boolean cercaDicotomica(int nums[], int valor)
	{
		int esq=0,dr=nums.length-1,centre;
		
		centre = (esq+dr)/2;
		while ((esq <= dr) && (nums[centre] != valor))
		{
			// si arribe aquí és perque el valor central no és el de recerca
			if (nums[centre] > valor)
				dr = centre - 1;
			else
				esq = centre + 1;
			centre = (esq+dr)/2;
		}
		if (nums[centre] == valor)
			return true;
		return false;
	}
}
