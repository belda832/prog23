import java.util.Scanner;

public class e3Optimitzada
{
	public static void main(String[] args) 
	{
		Scanner ent=new Scanner(System.in);
		boolean palindrome=true;
		
		System.out.println("Introduce una palabra");
		String palabra=ent.nextLine();
		
		int cont=0, cont1=palabra.length();
		//System.out.println(cont1);
		
		for(int i=0;i<(palabra.length()/2)+1;i++)
		{
			if(palabra.charAt(i)==palabra.charAt(cont1-1))
			{
				//palindrome=true;
				cont1-=1;
			}
			else	// si són diferents caracters
			{
				palindrome=false;
				break;	
			}		
		}
		
		if(palindrome)
			System.out.println("La palabra " + palabra + " es palíndroma");
		else
			System.out.println("La palabra " + palabra + " no es palíndroma");
	}
}
