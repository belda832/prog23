//4. Implemente un programa que reba una cadena S i una lletra X, i col·loque en majúscules cada ocurrència de X en S. (la funció ha de modificar la variable S). 
import java.util.Scanner;

public class e4
{
	public static void main(String[] args) 
	{
		//boolean encontrado=true;
		
		Scanner ent= new Scanner(System.in);
		
		System.out.println("Introduce una cadena de carácteres");		
		String s= ent.nextLine();
		
		System.out.println("Introduce el carácter que quieres cambiar a mayúsculas");	
		char x = ent.next().charAt(0);
		
		for(int i=0; i<s.length(); i++)
		{
			/*String c=c.valueOf(x);*/
			if( s.charAt(i) == x)
			{
				char y= Character.toUpperCase(x);    
				s=s.replace(x, y);
				break;	// fonamental, acabar el bucle, no fa falta seguir perque replace ja fa el canvi en totes les aparicions del caracter
			}
		}
		
		System.out.println(s);
	
	}
}
